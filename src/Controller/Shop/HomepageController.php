<?php

declare(strict_types=1);

namespace App\Controller\Shop;

use App\Entity\Taxonomy\Taxon;
use Entity\Category;
use Entity\Repository\CategoryRepository;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class HomepageController extends AbstractController
{


    public function indexAction(): Response
    {
        //dd($this->getDoctrine()->getRepository(Taxon::class)->findByCode('category')[0]->getChildren());
        return $this->render('@SyliusShop/Homepage/index.html.twig',[
            'categories' => $this->getDoctrine()->getRepository(Taxon::class)->findByCode('category')[0]->getChildren()
        ]);
    }

    public function customAction(): Response
    {
        return $this->render('custom.html.twig');
    }
}
