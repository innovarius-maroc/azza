<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Taxonomy\Taxon;
use FOS\RestBundle\View\View;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController
{
   /* public function indexAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $this->isGrantedOr403($configuration, ResourceActions::INDEX);
        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);
        $this->eventDispatcher->dispatchMultiple(ResourceActions::INDEX, $configuration, $resources);
        $view = View::create($resources);
        if ($configuration->isHtmlRequest()) {
            $view
                ->setTemplate($configuration->getTemplate(ResourceActions::INDEX . '.html'))
                ->setTemplateVar($this->metadata->getPluralName())
                ->setData([
                    'configuration' => $configuration,
                    'metadata' => $this->metadata,
                    'resources' => $resources,
                    $this->metadata->getPluralName() => $resources,
                ])
            ;
        }

//dump($this->getParentTaxon($request->attributes->get('slug')));
        /*dd($this->getDoctrine()->getRepository(Taxon::class)->findBy([
            'slug' => $this->getParentTaxon($request->attributes->get('slug'))
            ]
        ));
        return $this->viewHandler->handle($configuration, $view);
    }*/

    private function getParentTaxon(string $slug): string
    {
        $slugCount = count(explode('/', $slug));
        if ( $slugCount <= 2)
            return explode('/', $slug)[0];
        elseif ($slugCount > 2)
            return explode('/', $slug)[$slugCount-2];
    }
}
