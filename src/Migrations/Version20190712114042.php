<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190712114042 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Supplier ADD city_id INT NOT NULL');
        $this->addSql('ALTER TABLE Supplier ADD CONSTRAINT FK_625C0E288BAC62AF FOREIGN KEY (city_id) REFERENCES City (id)');
        $this->addSql('CREATE INDEX IDX_625C0E288BAC62AF ON Supplier (city_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Supplier DROP FOREIGN KEY FK_625C0E288BAC62AF');
        $this->addSql('DROP INDEX IDX_625C0E288BAC62AF ON Supplier');
        $this->addSql('ALTER TABLE Supplier DROP city_id');
    }
}
