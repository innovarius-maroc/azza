<?php


namespace App\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();


        // Configuration Sub menu
        $configurationSubmenu = $event->getMenu()->getChild('configuration');

        $configurationSubmenu
            ->addChild('cities',[
                'route' => 'app_admin_city_index'
            ])
            ->setLabel('Villes')
        ;


        // Supplier Submenu
        $supplierSubmenu = $menu
            ->addChild('new')
            ->setLabel('Fournisseurs')
        ;
        $supplierSubmenu
            ->addChild('new-subitem',[
                'route' => 'app_admin_supplier_index'
            ])
            ->setLabel('Liste des fournisseurs')
        ;
    }
}
